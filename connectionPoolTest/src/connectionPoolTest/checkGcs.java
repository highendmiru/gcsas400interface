package connectionPoolTest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class checkGcs {
	public void gcsInsert() {
		// DB2 Connection
		ConnectionManager db2conMgr = new DB2ConnectionManager();
		Connection db2conn = null;
		Statement db2stmt = null;
		ResultSet db2rs = null;
		
		checkGcs chk = new checkGcs();
		
		// DB2 query
		// GCT_STTS = N 인 것들을 가져온다
		String db2query_select = "SELECT GCT_CNTR_NO, GCT_SHP_NO, GCT_BL_NO, GCT_IVC_NO, GCT_ARR_DT, GCT_CC_DT, GCT_VND_CD FROM glvlggct WHERE GCT_STTS='N'";

		try {
			// 데이터베이스의 연결 설정
			db2conn = db2conMgr.getConnection();
			// Statement를 가져온다.
			db2stmt = db2conn.createStatement();
			// SQL문을 실행한다.
			db2rs = db2stmt.executeQuery(db2query_select);
			
			Log.out("gcsInsert() : " + db2query_select);
		
			while(db2rs.next()) {
				String gctCntrNo = db2rs.getString(1).replaceAll(" ", "");
				String gctShpNo = db2rs.getString(2).replaceAll(" ", "");
				String gctBlNo = db2rs.getString(3).replaceAll(" ", "");
				String gctIvcNo = db2rs.getString(4).replaceAll(" ", "");
				String gctArrDt = db2rs.getString(5).replaceAll(" ", "");
				String gctCcDate = db2rs.getString(6).replaceAll(" ", "");
				String gctVndCd = db2rs.getString(7).replaceAll(" ", "");
				
				// GCS에 없으면 insert 하자
				// checkGcsCT : GCS 에 cont_no 로 체크 해서 return 값이 0이면 i
				if(chk.checkGcsCT(gctCntrNo) == 0) {
					Log.out("insert Gcs CT will start");
					chk.insertGcsCT(gctCntrNo,gctBlNo,gctIvcNo,gctArrDt,gctCcDate,gctVndCd);
					Log.out("DB2 update will be start");
					chk.updateDB2("I", gctCntrNo, gctShpNo);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				db2rs.close();
				//Statement를 닫는다.
				db2stmt.close();
				//Connection을 풀로 복귀
				db2conMgr.freeConnection(db2conn);
			} catch(SQLException e) {}
		}
	}
	
	public void gcsAtaUpdate() {
		// DB2 Connection
		ConnectionManager db2conMgr = new DB2ConnectionManager();
		Connection db2conn = null;
		Statement db2stmt = null;
		ResultSet db2rs = null;
		
		checkGcs chk = new checkGcs();
		
		// DB2 query
		// GCT_STTS = N 인 것들을 가져온다
		String db2query_select = "SELECT GCT_CNTR_NO, GCT_SHP_NO, GCT_ARR_DT, GCT_VND_CD FROM glvlggct WHERE GCT_STTS='I'";

		try {
			Log.out("gcsAtaUpdate() : " + db2query_select);
			// 데이터베이스의 연결 설정
			db2conn = db2conMgr.getConnection();
			// Statement를 가져온다.
			db2stmt = db2conn.createStatement();
			// SQL문을 실행한다.
			db2rs = db2stmt.executeQuery(db2query_select);
		
			while(db2rs.next()) {
				String gctCntrNo = db2rs.getString(1).replaceAll(" ", "");
				String gctShpNo = db2rs.getString(2).replaceAll(" ", "");
				String gctArrDt = db2rs.getString(3).replaceAll(" ", "");
				String gctVndCd = db2rs.getString(4).replaceAll(" ", "");
				
				// ATA update 하자
				// checkGcsAta : gcs 에서 ata 체크해서 반환. 그런데 길이가 2이하라면 ata date가 없다고 가정
				// checkDB2Arr : DB2 에서 arr date 체크해서 반환. 길이가 2 이상이면 값이 있다고 가정
				if(chk.checkGcsAta(gctCntrNo).length() < 2 && chk.checkDB2Arr(gctCntrNo, gctShpNo).length() > 2) {
					chk.updateGcsAta(gctCntrNo,gctArrDt,gctVndCd);
					chk.updateDB2("U", gctCntrNo, gctShpNo);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				db2rs.close();
				//Statement를 닫는다.
				db2stmt.close();
				//Connection을 풀로 복귀
				db2conMgr.freeConnection(db2conn);
			} catch(SQLException e) {}
		}
	}
	
	public void gcsCusUpdate() {
		// DB2 Connection
		ConnectionManager db2conMgr = new DB2ConnectionManager();
		Connection db2conn = null;
		Statement db2stmt = null;
		ResultSet db2rs = null;
		
		checkGcs chk = new checkGcs();
		
		// DB2 query
		// GCT_STTS = N 인 것들을 가져온다
		String db2query_select = "SELECT GCT_CNTR_NO, GCT_SHP_NO FROM glvlggct WHERE GCT_STTS='U'";

		try {
			Log.out("gcsCusUpdate() : " + db2query_select);
			// 데이터베이스의 연결 설정
			db2conn = db2conMgr.getConnection();
			// Statement를 가져온다.
			db2stmt = db2conn.createStatement();
			// SQL문을 실행한다.
			db2rs = db2stmt.executeQuery(db2query_select);
		
			while(db2rs.next()) {
				String gctCntrNo = db2rs.getString(1).replaceAll(" ", "");
				String gctShpNo = db2rs.getString(2).replaceAll(" ", "");
				
				// CUSTOMS update 하자
				// chkGcsGr : gcs 에서 customs 체크해서 반환
				// chkDB2Cc : DB2 에 Cc date 체크해서 반환.
				if(chk.checkGcsCus(gctCntrNo).toString() == "N" && chk.checkDB2Cc(gctCntrNo, gctShpNo).length() > 2) {
					chk.updateGcsCus(gctCntrNo);
					chk.updateDB2("C", gctCntrNo, gctShpNo);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				db2rs.close();
				//Statement를 닫는다.
				db2stmt.close();
				//Connection을 풀로 복귀
				db2conMgr.freeConnection(db2conn);
			} catch(SQLException e) {}
		}
	}
	
	public void DB2RcvUpdate() {
		// DB2 Connection
		ConnectionManager db2conMgr = new DB2ConnectionManager();
		Connection db2conn = null;
		Statement db2stmt = null;
		ResultSet db2rs = null;
	
		checkGcs chk = new checkGcs();
		
		// DB2 query
		// GCT_STTS = N 인 것들을 가져온다
		String db2query_select = "SELECT GCT_CNTR_NO, GCT_SHP_NO FROM glvlggct WHERE GCT_STTS='C'";

		try {
			Log.out("DB2RcvUpdate() : " + db2query_select);
			// 데이터베이스의 연결 설정
			db2conn = db2conMgr.getConnection();
			// Statement를 가져온다.
			db2stmt = db2conn.createStatement();
			// SQL문을 실행한다.
			db2rs = db2stmt.executeQuery(db2query_select);
		
			while(db2rs.next()) {
				String gctCntrNo = db2rs.getString(1).replaceAll(" ", "");
				String gctShpNo = db2rs.getString(2).replaceAll(" ", "");
				
				// GR잡히면 DB2 에 update 하자
				if(chk.checkGcsGr(gctCntrNo) == "Y" && chk.checkDB2Rcv(gctCntrNo, gctShpNo).length() > 2) {
					chk.updateDB2Rcv(gctCntrNo, gctShpNo);
					chk.updateDB2("D", gctCntrNo, gctShpNo);
				}	
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				db2rs.close();
				//Statement를 닫는다.
				db2stmt.close();
				//Connection을 풀로 복귀
				db2conMgr.freeConnection(db2conn);
			} catch(SQLException e) {}
		}
	}
	
	public int checkGcsCT(String cont_no) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int count = 0;
		
		String query = "SELECT count(cont_no) FROM asn_hd WHERE cont_no = '" + cont_no + "'";
		
		try {
			Log.out("checkGcsCT() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();

			// Statement를 가져온다.
			stmt = conn.createStatement();

			// SQL문을 실행한다.
			rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				count = rs.getInt(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				rs.close();
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
		return count;
	}
	
	public String checkGcsGr(String cont_no) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String count = null;
		
		String query = "SELECT gr_sts FROM asn_hd WHERE cont_no = '" + cont_no + "'";
		
		try {
			Log.out("checkGcsGr() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();

			// Statement를 가져온다.
			stmt = conn.createStatement();

			// SQL문을 실행한다.
			rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				count = rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				rs.close();
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
		return count;
	}
	
	public String checkGcsCus(String cont_no) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String count = null;
		
		String query = "SELECT customs FROM asn_hd WHERE cont_no = '" + cont_no + "'";
		
		try {
			Log.out("checkGcsCus() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();

			// Statement를 가져온다.
			stmt = conn.createStatement();

			// SQL문을 실행한다.
			rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				count = rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				rs.close();
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
		return count;
	}
	
	public String checkGcsAta(String cont_no) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String count = null;
		
		String query = "SELECT ata_dt FROM asn_hd WHERE cont_no = '" + cont_no + "'";
		
		try {
			Log.out("checkGcsAta() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();

			// Statement를 가져온다.
			stmt = conn.createStatement();

			// SQL문을 실행한다.
			rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				count = rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				rs.close();
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
		return count;
	}
	
	public String checkDB2Arr(String contNo, String shpNo) {
		ConnectionManager conMgr = new DB2ConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String count = null;
		
		String query = "SELECT gct_arr_dt FROM glvlggct WHERE gct_cntr_no = '" + contNo + "' and gct_shp_no = '" + shpNo + "'";
		
		try {
			Log.out("checkDB2Arr() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();

			// Statement를 가져온다.
			stmt = conn.createStatement();

			// SQL문을 실행한다.
			rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				count = rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				rs.close();
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
		return count;
	}

	public String checkDB2Rcv(String contNo, String shpNo) {
		ConnectionManager conMgr = new DB2ConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String count = null;
		
		String query = "SELECT gct_rcv_dt FROM glvlggct WHERE gct_cntr_no = '" + contNo + "' and gct_shp_no = '" + shpNo + "'";
		
		try {
			Log.out("checkDB2Rcv() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();

			// Statement를 가져온다.
			stmt = conn.createStatement();

			// SQL문을 실행한다.
			rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				count = rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				rs.close();
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
		return count;
	}
	public String checkDB2Cc(String contNo, String shpNo) {
		ConnectionManager conMgr = new DB2ConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String count = null;
		
		String query = "SELECT gct_cc_date FROM glvlggct WHERE gct_cntr_no = '" + contNo + "' and gct_shp_no = '" + shpNo + "'";
		
		try {
			Log.out("checkDB2Cc() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();

			// Statement를 가져온다.
			stmt = conn.createStatement();

			// SQL문을 실행한다.
			rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				count = rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//ResultSet을 닫는다.
				rs.close();
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
		return count;
	}
	
	public void updateGcsAta(String gctCntrNo, String gctArrDt,String gctVndCd) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		
		String query = "UPDATE asn_hd SET ata_dt = '" + gctArrDt + "', detn_dt = to_char(to_date('" + gctArrDt + "','YYYYMMDD')+58,'YYYYMMDD'), vessel = '" + gctVndCd + "' WHERE cont_no = '" + gctCntrNo +"'";
		
		try {
			Log.out("updateGcsAta() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();
			// Statement를 가져온다.
			stmt = conn.createStatement();
			// SQL문을 실행한다.
			stmt.executeUpdate(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}		
	}
	
	public void updateGcsCus(String gctCntrNo) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		
		String query = "UPDATE asn_hd SET customs = 'Y' WHERE cont_no = '" + gctCntrNo + "'";
		
		try {
			Log.out("updateGcsCus() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();
			// Statement를 가져온다.
			stmt = conn.createStatement();
			// SQL문을 실행한다.
			stmt.executeUpdate(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}		
	}
	
	public void updateDB2Rcv(String gctCntrNo, String gctShpNo) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		
		String query = "UPDATE glvlggct SET GCT_RCV_USR = 'GLOVIS', GCT_RCV_DT = to_char(sysdate,'YYYYMMDD') WHERE GCT_CNTR_NO = '" + gctCntrNo + "' and GCT_SHP_NO = '" + gctShpNo + "'";
		
		try {
			Log.out("updateDb2Rcv() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();
			// Statement를 가져온다.
			stmt = conn.createStatement();
			// SQL문을 실행한다.
			stmt.executeUpdate(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
	}
	
	public void updateDB2(String args, String gctCntrNo, String gctShpNo) {
		ConnectionManager conMgr = new DB2ConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		
		String query = "UPDATE glvlggct SET GCT_STTS = '" + args + "' WHERE GCT_CNTR_NO = '" + gctCntrNo + "' and GCT_SHP_NO = '" + gctShpNo + "'";
		
		try {
			Log.out("updateDB2() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();
			// Statement를 가져온다.
			stmt = conn.createStatement();
			// SQL문을 실행한다.
			stmt.executeUpdate(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
	}
		
	public void insertGcsCT(String gctCntrNo,String gctBlNo,String gctIvcNo,String gctArrDt,String gctCcDate,String gctVndCd) {
		ConnectionManager conMgr = new OracleConnectionManager();
		Connection conn = null;
		Statement stmt = null;
		
		String query = "INSERT INTO ASN_HD (CONT_NO,BOL_NO,INVO_NO,ATA_DT,DETN_DT,CUSTOMS,VESSEL) VALUES('"
		+ gctCntrNo + "','" // CONT_NO 
		+ gctBlNo + "','"
		+ gctIvcNo + "','"
		+ gctArrDt + "',"
		+ " to_char(to_date('" + gctArrDt + "','YYYYMMDD')+58,'YYYYMMDD'),"
		+ " decode('" + gctCcDate + "', ' ','N','Y'), '"
		+ gctVndCd + "')";
		
		try {
			Log.out("insertGcsCT() : " + query);
			// 데이터베이스의 연결 설정
			conn = conMgr.getConnection();
			// Statement를 가져온다.
			stmt = conn.createStatement();
			// SQL문을 실행한다.
			stmt.executeUpdate(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//Statement를 닫는다.
				stmt.close();
				//Connection을 풀로 복귀
				conMgr.freeConnection(conn);
			} catch(SQLException e) {}
		}
	}
	
	
}// 클래스의 끝.