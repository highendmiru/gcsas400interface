package connectionPoolTest;

import connectionPoolTest.Log;

public class GcsAs400Interface {
	public static void main(String[] args) {
		checkGcs chk = new checkGcs();
		
		Log.out("gcs Insert is startd");
		chk.gcsInsert();
		Log.out("gcs Insert is done");
		
		Log.out("gcs ata update is startd");
		chk.gcsAtaUpdate();
		Log.out("gcs ata update is done");
		
		Log.out("gcs custom update is startd");
		chk.gcsCusUpdate();
		Log.out("gcs custom update is done");
		
		Log.out("DB2 rcv update is startd");
		chk.DB2RcvUpdate();
		Log.out("DB2 rcv update is done");
	}
}
