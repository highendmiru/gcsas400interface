AS400 데이터를 Gcs 서버로 interface 하는 프로그램

1. DB 접속 정보
C:\jdbc 폴더 내에 db2.properties, oracle.properties 파일에 DB 접속 정보 저장
내용은 아래와 같다.





*  dbServer =                 // 접속 DB의 IP address 

*  port = 1521                // 접속 DB의 port 정보

* dbName = XE                 // DB name. oracle 경우에는 SID나 service name
 
* userID =                    // userID
 
* passwd =                    // user password
 
* maxConn = 20                // 허용 접속 수
 
* initConn = 2                // 최초 생성 접속 수
 
* maxWait = 5                 // 허용 접속 수를 초과 했을 경우 대기 시간

